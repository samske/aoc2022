val sums = ArrayList<Int>()
var last = 0
java.nio.file.Files.lines(java.nio.file.Paths.get("day1.txt")).forEach{
    if (it.isEmpty()) {
        sums.add(last)
        last = 0
    } else {
        last += it.toInt()
    }
}
sums.add(last)

println(sums.max())

data class Podium(val first: Int, val second: Int, val third: Int)
println(sums.fold(Podium(0, 0, 0)){podium, value ->
    if (value < podium.third) {
        podium
    } else if (value < podium.second) {
        Podium(podium.first, podium.second, value)
    } else if (value < podium.first) {
        Podium(podium.first, value, podium.second)
    } else {
        Podium(value, podium.first, podium.second)
    }
}.let{it.first + it.second + it.third})

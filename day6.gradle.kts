listOf(4, 14).forEach{length ->
    sequence{
        java.nio.file.Files.newBufferedReader(java.nio.file.Paths.get("day6.txt")).use{reader ->
            while(true) {
                val next = reader.read()
                if (next == -1) {
                    break
                }
                yield(next.toChar())
            }
        }
    }.runningFold(0 to listOf<Char>()){(index, prev), char ->
        (index + 1) to ((if (prev.size == length) prev.drop(1) else prev) + char)
    }.drop(length).find{(index, seq) -> seq.toSet().size == seq.size}!!.let{println(it.first)}
}

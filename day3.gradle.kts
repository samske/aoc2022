import kotlin.streams.asSequence

fun lines() = java.nio.file.Files.lines(java.nio.file.Paths.get("day3.txt")).asSequence()
fun Sequence<Set<Char>>.score() = flatMap{it}.map{letter ->
    when (letter) {
        in 'a'..'z' -> letter - 'a' + 1
        in 'A'..'Z' -> letter - 'A' + 27
        else -> throw Exception("unexpected ${letter}")
    }
}.sum()

println(lines().map{line ->
    val (a, b) = line.chunked(line.length / 2)
    assert(a + b == line)
    val set = a.toSortedSet()
    set.retainAll(b.toList())
    set
}.score())

println(lines().chunked(3).map{(a, b, c) ->
    val set = a.toSortedSet()
    set.retainAll(b.toList())
    set.retainAll(c.toList())
    set
}.score())

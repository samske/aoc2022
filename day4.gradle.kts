val regex = Regex("(.*)-(.*),(.*)-(.*)")

fun tuples() = java.nio.file.Files.lines(java.nio.file.Paths.get("day4.txt")).map{line ->
    regex.matchEntire(line)!!.groupValues.drop(1).map{it.toInt()}
}

println(tuples().filter{(from1, to1, from2, to2) ->
    (from1 <= from2 && to2 <= to1) || (from2 <= from1 && to1 <= to2)
}.count())

println(tuples().filter{(from1, to1, from2, to2) ->
    (from1 <= to2 && from2 <= to1) || (from2 <= to1 && from1 <= to2)
}.count())

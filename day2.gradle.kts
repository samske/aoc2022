enum class Play {
    ROCK, PAPER, SCISSORS
}
enum class Outcome {
    LOSS, DRAW, WIN
}

val regex = Regex("(.) (.)")
fun score(block: (Play, String)->Pair<Play, Outcome>) =
    java.nio.file.Files.lines(java.nio.file.Paths.get("day2.txt")).mapToInt{line ->
        regex.matchEntire(line)!!.groupValues.drop(1).let{(otherPlayStr, second) ->
            val otherPlay = when(otherPlayStr) {
                "A" -> Play.ROCK
                "B" -> Play.PAPER
                "C" -> Play.SCISSORS
                else -> throw Exception("unexpected ${otherPlayStr}")
            }
            val (selfPlay, outcome) = block(otherPlay, second)
            when(selfPlay) {
                Play.ROCK -> 1
                Play.PAPER -> 2
                Play.SCISSORS -> 3
            } + when(outcome) {
                Outcome.LOSS -> 0
                Outcome.DRAW -> 3
                Outcome.WIN -> 6
            }
        }
    }.sum()

println(score{otherPlay, selfPlayStr ->
    val selfPlay = when(selfPlayStr) {
        "X" -> Play.ROCK
        "Y" -> Play.PAPER
        "Z" -> Play.SCISSORS
        else -> throw Exception("unexpected ${selfPlayStr}")
    }
    val outcome = when (otherPlay to selfPlay) {
        Play.PAPER to Play.ROCK -> Outcome.LOSS
        Play.PAPER to Play.SCISSORS -> Outcome.WIN
        Play.PAPER to Play.PAPER -> Outcome.DRAW
        Play.ROCK to Play.PAPER -> Outcome.WIN
        Play.ROCK to Play.SCISSORS -> Outcome.LOSS
        Play.ROCK to Play.ROCK -> Outcome.DRAW
        Play.SCISSORS to Play.PAPER -> Outcome.LOSS
        Play.SCISSORS to Play.ROCK -> Outcome.WIN
        Play.SCISSORS to Play.SCISSORS -> Outcome.DRAW
        else -> throw Exception("unexpected ${otherPlay} ${selfPlay}")
    }
    selfPlay to outcome
})

println(score{otherPlay, outcomeStr ->
    val outcome = when(outcomeStr) {
        "X" -> Outcome.LOSS
        "Y" -> Outcome.DRAW
        "Z" -> Outcome.WIN
        else -> throw Exception("unexpected ${outcomeStr}")
    }
    val selfPlay = when (otherPlay to outcome) {
        Play.PAPER to Outcome.LOSS -> Play.ROCK
        Play.PAPER to Outcome.WIN -> Play.SCISSORS
        Play.PAPER to Outcome.DRAW -> Play.PAPER
        Play.ROCK to Outcome.WIN -> Play.PAPER
        Play.ROCK to Outcome.LOSS -> Play.SCISSORS
        Play.ROCK to Outcome.DRAW -> Play.ROCK
        Play.SCISSORS to Outcome.LOSS -> Play.PAPER
        Play.SCISSORS to Outcome.WIN -> Play.ROCK
        Play.SCISSORS to Outcome.DRAW -> Play.SCISSORS
        else -> throw Exception("unexpected ${otherPlay} ${outcome}")
    }
    selfPlay to outcome
})

val crateRegex = "(?:   |\\[(?<crate>.)\\]) ?"
val cratesRegex = "(?:${crateRegex})+"
val bottomRegex = " . (?:  . )*"
val emptyRegex = ""
val moveRegex = "move (?<quantity>.*) from (?<from>.*) to (?<to>.*)"
val lineRegex = "(?<crates>${cratesRegex})|(?<bottom>${bottomRegex})|(?<empty>${emptyRegex})|(?<move>${moveRegex})"

fun day(reverse: Boolean) {
    val stacks = mutableMapOf<Int, MutableList<String>>()
    java.nio.file.Files.lines(java.nio.file.Paths.get("day5.txt")).forEach{line->
        val groups = Regex(lineRegex).matchEntire(line)!!.groups
        if (groups["crates"] != null) {
            Regex(crateRegex).findAll(line).forEachIndexed{index, match ->
                val crate = match.groups["crate"]
                if (crate != null) {
                    stacks.getOrPut(index + 1, ::mutableListOf).add(0, crate.value)
                }
            }
        } else if (groups["move"] != null) {
            val quantity = groups["quantity"]!!.value.toInt()
            val from = stacks.get(groups["from"]!!.value.toInt())!!
            val to = stacks.get(groups["to"]!!.value.toInt())!!
            val forklift = mutableListOf<String>()
            (1..quantity).forEach{
                forklift.add(from.removeLast())
            }
            if (reverse) {
                forklift.reverse()
            }
            to.addAll(forklift)
        }
    }
    println(stacks.toSortedMap().values.map{it.last()}.joinToString(""))
}
day(false)
day(true)
